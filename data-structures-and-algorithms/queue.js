let collection = [];


// Write the queue functions below.


// 1. Output all the elements of the queue
const print = () => {
	return collection;
}

console.log(print());

// 2. Adds element to the rear of the queue
let index1 = 0
let enqueue = (val) => {
	collection[index1] = val
	index1++
	return collection;
}

// 3. Removes element from the front of the queue

const dequeue = () => {
	let newCollection = []
	let index2 = 0;
	for(let i = 1; i < collection.length; i++) {
		newCollection[index2] = collection[i]
		index2++;
	}
	index1--
	collection = newCollection;
	return newCollection;
}


// 4. Show element at the front

const front = () => {
	return collection[0]
}

// 5. Show the total number of elements

const size = () => {
	return collection.length
}
console.log(size())

// 6. Outputs a Boolean value describing whether queue is empty or not

const isEmpty = () => {
	return (size() > 0) ? false : true; 
}

module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};